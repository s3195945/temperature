package nl.utwente.di.bookQuote;

public class Counter {
    public double convertTemp(String temp) {
        int celsius = Integer.parseInt (temp);
        return (9.0/5.0) * celsius + 32;
    }
}
